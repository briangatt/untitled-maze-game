#include <ctype.h>
#include <deque>

namespace octet {
  
  // --

  // Raises base to the power of index
  int pow(int base, int index) {
    int rvalue = 1;

    for (int i = 0; i < index; ++i) {
      rvalue *= base;
    }

    return rvalue;
  };

  // Converts the ASCII character representation into a decimal
  // integer according to the base provided
  int asciiToInt(const char ascii, const int base = 10) {
    // Implemented via a table-lookup mechanism. Avoided using ASCII
    // table codes due to other symbols in between digits and letters.
    static const char* _hex = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    if (base <= 36) {
      for (int i = 0; i < base; ++i) {
        if (_hex[i] == ascii || tolower(_hex[i]) == ascii) {
          return i;
        }
      }
    }

    return -1;
  };

  // Convenience function which converts a base-32 character
  // into a decimal integer
  int base32ToInt(const char base32) {
    return asciiToInt(base32, 32);
  };

  // Converts a string of particular length into a decimal integer
  // according to the provided base
  int asciiToInt(const char *string, int length, const int base = 10) {
    int rvalue = 0;

    for (int i = 0; i < length; ++i) {
      rvalue += asciiToInt(string[i], base) * pow(base, (length - 1 - i));
    }

    return rvalue;
  };

  // Convenience function which converts a base-32 string
  // into a decimal integer
  int base32ToInt(const char *base32, int length) {
    return asciiToInt(base32, length, 32);
  };

  // --

  // A template class which describes a point in space
  template <typename T>
  struct Point2D {
    T x;
    T y;
  
    explicit Point2D(T x = 0, T y = 0):
      x(x),
      y(y) {
    };
  
    // Representation of the zero Point i.e. (0, 0)
    static const Point2D& zero() {
      static const Point2D zero(0,0);
      return zero;
    };
  };

  // Point equality operator
  template <typename T>
  bool operator==(const Point2D<T>& lhs, const Point2D<T>& rhs) {
      return lhs.x == rhs.x && lhs.y == rhs.y;
  };

  // Convenience typedefs for discrete and continuous points
  typedef Point2D<int> Point2Di;
  typedef Point2D<float> Point2Df;
  
  // --

  // Singleton Pattern
  // Intended to be publicly extended by clients
  // via the curiously recurring template pattern
  // Reference: http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
  template <typename T>
  class Singleton {
  private:
    Singleton(const Singleton&);
    Singleton& operator=(const Singleton&);

  protected:
    Singleton() {
    };

  public:
    virtual ~Singleton() {
    };

    static T& getInstance() {
      // Assumes type T has a no-arg default constructor
      // accessible by Singelton class
      static T instance;
      return instance;
    };
  };

  // --

  // A level description
  class LevelDefinition {
  private:
    static const int _headerLength = 5; 
  
    const char* _definition;

    int _width;
    int _height;
    Point2Di _playerOrigin;

    int _properties;

    LevelDefinition() :
     _definition(NULL),
     _width(0),
     _height(0),
     _playerOrigin(Point2Di::zero()),
     _properties(0) {
    };
  
    // Retrieves a particular tile from the level definition
    char getTile(const Point2Di& position) const {
      return _definition[(_headerLength + (_width * position.y) + position.x)];
    };
  
    bool isConnected(const Point2Di& position) const {
      return isConnected(position, North) ||
               isConnected(position, East) ||
               isConnected(position, South) ||
               isConnected(position, West);
    };
  
  public:
    ~LevelDefinition() {
    };
  
    // Cardinal compass directions
    enum Direction { North = 0x1, East = 0x2 , South = 0x4, West = 0x8 };
    // Level properties
    enum Property { Dark = 0x1, Retraversable = 0x2 };

    const char* getDefinition() const {
      return _definition;
    };
  
    int getWidth() const {
      return _width;
    };
  
    int getHeight() const {
      return _height;
    };
  
    const Point2Di& getPlayerOrigin() const {
      return _playerOrigin;
    };
  
    bool isConnected(const Point2Di& position, Direction direction) const {
      return (base32ToInt(getTile(position)) & direction) == direction;
    };
  
    bool getSpecialBit(const Point2Di& position) const {
      return (base32ToInt(getTile(position)) & 0x10) == 0x10;
    };

    bool hasProperty(Property propertyKey) const {
      return (_properties & propertyKey) == propertyKey;
    };
  
    // States whether the contained level definition format is valid
    bool isValid() const {
      return _width > 0 && _height > 0 &&
             _playerOrigin.x > -1 && _playerOrigin.x < _width &&
		         _playerOrigin.y > -1 && _playerOrigin.y < _height &&
             strlen(_definition) == (_width * _height) + _headerLength;// &&
             //isConnected(_playerOrigin);
    };
  
    // Named constructor pattern for creating LevelDefinitions based on a
    // string.
    static LevelDefinition parse(const char* levelDefinition) {
      LevelDefinition level;
    
      int definitionLength = strlen(levelDefinition);
    
      // Protocol Specification:
      // WHXYP...
      // Where each character labelled accordingly represents:
      // - W = width
      // - H = height
      // - X = Player starting X position
      // - Y = Player starting Y position
      // - P = Bitmask specifying level properties
      // - ... = level tiles connections

      level._definition = levelDefinition;
      level._width = (definitionLength >= 1 ? base32ToInt(levelDefinition[0]) : -1);
      level._height = (definitionLength >= 2 ? base32ToInt(levelDefinition[1]) : -1);
      level._playerOrigin = (definitionLength >= 4 ? 
        Point2Di(base32ToInt(levelDefinition[2]), base32ToInt(levelDefinition[3])) :
        Point2Di(-1, -1)
      );
      level._properties = (definitionLength >= 5 ? base32ToInt(levelDefinition[4]) : -1);
    
      return level;
    };
  
    static bool isValid(char* levelDefinition) {
      return parse(levelDefinition).isValid();
    };
  };
  
  // --

  // Representation of a Tile in a level.
  // Contains accessors for neighbours.
  class Tile {
  private:
    bool _visited;
    bool _retraversable;
    Tile* _neighbours[4];

    mutable void* _userData;
  
    static unsigned int getIndex(LevelDefinition::Direction direction) {
      switch (direction) {
      case LevelDefinition::North: return 0;
      case LevelDefinition::East: return 1;
      case LevelDefinition::South: return 2;
      case LevelDefinition::West: return 3;
      };
    
      return -1;
    };
  
  public:
    Tile() :
      _visited(false),
      _retraversable(false),
      _userData(NULL) {
      for (int i = 0; i < 4; ++i) {
        _neighbours[i] = NULL;
      }
    };
  
    ~Tile() {
    };
  
    bool isVisited() const {
      return _visited;
    };
  
    void setVisited(bool visited) {
      _visited = visited;
    };
  
    bool isRetraversable() const {
      return _retraversable;
    };
  
    void setRetraversable(bool retraversable) {
      _retraversable = retraversable;
    };

    const Tile* getNeighbour(LevelDefinition::Direction direction) const {
      return _neighbours[getIndex(direction)];
    };
  
    Tile* getNeighbour(LevelDefinition::Direction direction) {
      return _neighbours[getIndex(direction)];
    };
  
    void setNeighbour(Tile* neighbour, LevelDefinition::Direction direction) {
      _neighbours[getIndex(direction)] = neighbour;
    };

    void* getUserData() const {
      return _userData;
    };
    
    void setUserData(void* data) const {
      _userData = data;
    };
  
    bool isConnected() const {
      for (int i = 0; i < 4; ++i) {
        if (_neighbours[i] != NULL) {
          return true;
        }
      }
    
      return false;
    };
  };
  
  // --

  // Player abstraction
  class Player {
  private:
    Point2Di _position;
  
  public:
    explicit Player(const Point2Di& position = Point2Di::zero()) :
      _position(position) {
    };
  
    ~Player() {
    };
  
    const Point2Di& getPosition() const {
      return _position;
    };
  
    void setPosition(const Point2Di& position) {
      _position = position;
    };
  
    void move(LevelDefinition::Direction direction) {
      switch (direction) {
      case LevelDefinition::North: --_position.y; break;
      case LevelDefinition::South: ++_position.y; break;
      case LevelDefinition::East: ++_position.x; break;
      case LevelDefinition::West: --_position.x; break;
      };
    };
  };

  // --

  // A stack wrapper over std::deque which ensures that
  // only a specific amount of elements are at one time
  // in the stack. If the capacity is exceeded, old values
  // are discarded.
  template <typename T>
  class BoundedStack {
  private:
    std::deque<T> _stack;
    size_t _capacity;
  
  public:
    typedef T value_type;

    BoundedStack(size_t capacity) :
      _stack(capacity + 1),
      _capacity(capacity) {
    };
  
    ~BoundedStack() {
    };
  
    bool push(const value_type& item, bool force = true) {
      if (force && size() == capacity()) {
        _stack.pop_front();
	    }
	
	    if (size() != capacity()) {
	      _stack.push_back(item);
	      return true;
	    }
	
	    return false;
    };
  
    void pop() {
      _stack.pop_back();
    };
  
    const value_type& peek() const {
      return _stack.back();
    };
  
    value_type& peek() {
      return _stack.back();
    };
  
    const size_t capacity() const {
      return _capacity;
    };
  
    const size_t size() const {
      return _stack.size();
    };

    void resize(size_t newSize) {
      if (newSize != _capacity) {
        _stack.resize(newSize);
        _capacity = newSize;
      };
    };
  
    const bool empty() const {
      return _stack.empty();
    };

    void clear() {
      return _stack.clear();
    };
  };
  
  // --

  // Command Pattern abstract-base class
  class Command {
  public:
    virtual ~Command() {
    };

    virtual void operator()() = 0;
    virtual void undo() = 0;
  };
  
  // --

  // Dungeon Representation
  class Dungeon {
  private:
    Player _player;
    Tile* _tileMap;
  
    int _width;
    int _height;
  
    bool _isDark;

    // Represents a move done by the player
    // No need for inheritance since we are using it only once in our case
    class Move /*: public Command*/ {
    private:
      Dungeon* _outerType;
	    LevelDefinition::Direction _direction;

      bool _visited;
	
	    static LevelDefinition::Direction reverse(LevelDefinition::Direction direction) {
	      switch (direction) {
	      case LevelDefinition::North: return LevelDefinition::South;
	      case LevelDefinition::West: return LevelDefinition::East;
	      case LevelDefinition::South: return LevelDefinition::North;
	      case LevelDefinition::East: return LevelDefinition::West;
	      };
	  
	      return LevelDefinition::North;
	    };
	
    public:
      // Default constructor is made available in
      // order to use the Move type in containers without
      // making use of pointers, shared_ptr and/or the heap.
      // Alas this makes such an instance unusable.
	    Move() :
	      _outerType(NULL),
	      _direction(LevelDefinition::North),
        _visited(false) {
	    };
	
      Move(Dungeon* outerType, LevelDefinition::Direction direction) :
	      _outerType(outerType),
	      _direction(direction),
        _visited(false) {
	    };
	
	    ~Move() {
	    };

	    void operator()() {
	      _outerType->getPlayer().move(_direction);

        Tile& tile = _outerType->getTile(_outerType->getPlayer().getPosition());
        _visited = tile.isVisited();
        tile.setVisited(true);

        ++_outerType->_moveCount;
	    };
	
	    void undo() {
	      _outerType->getTile(_outerType->getPlayer().getPosition()).setVisited(_visited);
	      _outerType->getPlayer().move(reverse(_direction));
        
        // An undo still counts as a move
        ++_outerType->_moveCount;
	    };
    };
  
    BoundedStack<Move> _moves;
    unsigned int _undoMovesLeft;
    unsigned int _moveCount;
  
    Dungeon(
      int width,
      int height,
      bool isDark,
      const Point2Di& playerPosition,
      unsigned int maxUndos = getMaxUndos()
    ) :
      _player(playerPosition),
      _tileMap(new Tile[width * height]),
      _width(width),
      _height(height),
      _isDark(isDark),
      _moves(maxUndos),
      _undoMovesLeft(maxUndos),
      _moveCount(0) {
    };

    static unsigned int getMaxUndos() {
      return 3;
    };
  
    static bool isBlocked(const Tile* tile) {
      return tile == NULL || (tile->isVisited() && !tile->isRetraversable());
    };
  
    bool isBlocked(const Tile& tile) const {
      const Tile* north = tile.getNeighbour(LevelDefinition::North);
      const Tile* east = tile.getNeighbour(LevelDefinition::East);
      const Tile* south = tile.getNeighbour(LevelDefinition::South);
      const Tile* west = tile.getNeighbour(LevelDefinition::West);
    
      return isBlocked(north) && isBlocked(east) && isBlocked(south) && isBlocked(west);
    };
  
    Tile& getTile(const Point2Di& position) {
      return _tileMap[position.x + (position.y * getWidth())];
    };
  
    Player& getPlayer() {
      return _player;
    };
  
  public:
    // Dungeon constructor based on a previously defined level definition
    Dungeon(const LevelDefinition& definition, unsigned int maxUndos = getMaxUndos()) :
      _player(definition.getPlayerOrigin()),
      _tileMap(NULL),
      _width(definition.getWidth()),
      _height(definition.getHeight()),
      _isDark(false),
      _moves(maxUndos),
      _undoMovesLeft(maxUndos),
      _moveCount(0) {
    
      reset(definition, maxUndos);

    };
     
    // For efficient memory reuse
    void reset(const LevelDefinition& definition, unsigned int maxUndos = getMaxUndos()) {

      _player.setPosition(definition.getPlayerOrigin());
      _width = definition.getWidth();
      _height = definition.getHeight();
      _isDark = definition.hasProperty(LevelDefinition::Dark);
      
      delete[] _tileMap;
      // The tilemap is represented as a linear structure. Helper methods
      // exist which allow accessing tiles in an (X, Y) format.
      _tileMap = new Tile[definition.getWidth() * definition.getHeight()];

      _moves.resize(maxUndos);
      _moves.clear();
      
      _undoMovesLeft = maxUndos;
      _moveCount = 0;

	    Point2Di position;
      for (position.y = 0; position.y < definition.getHeight(); ++position.y) {
        for (position.x = 0; position.x < definition.getWidth(); ++position.x) {
          Tile& tile = getTile(position);
        
          // Assumption: if tile A connects with tile B, then tile B
          // connects with tile A => undirected graph.
        
          if (definition.isConnected(position, LevelDefinition::North) && (position.y - 1) >= 0) {
            Tile& northTile = getTile(Point2Di(position.x, (position.y - 1)));
          
            northTile.setNeighbour(&tile, LevelDefinition::South);
            tile.setNeighbour(&northTile, LevelDefinition::North);
          }
        
          if (definition.isConnected(position, LevelDefinition::West) && (position.x - 1) >= 0) {
            Tile& westTile = getTile(Point2Di((position.x - 1), position.y));
          
            westTile.setNeighbour(&tile, LevelDefinition::East);
            tile.setNeighbour(&westTile, LevelDefinition::West);
          }

          tile.setRetraversable(definition.hasProperty(LevelDefinition::Retraversable) && definition.getSpecialBit(position));
        }
      }
	
	    getTile(getPlayer().getPosition()).setVisited(true);

    };
  
    ~Dungeon() {
      delete[] _tileMap;
    };
  
    int getWidth() const {
      return _width;
    };
  
    int getHeight() const {
      return _height;
    };

    unsigned int getMoveCount() const {
      return _moveCount;
    };
  
    unsigned int getAvailableUndoCount() const {
      return _undoMovesLeft;
    };

    const Tile& getTile(const Point2Di& position) const {
      // Translate the (X, Y) format into the linear equivalent
      return _tileMap[position.x + (position.y * getWidth())];
    };
  
    const Player& getPlayer() const {
      return _player;
    };
  
    bool movePlayer(LevelDefinition::Direction direction) {
      Tile& currentTile = getTile(getPlayer().getPosition());
      Tile* nextTile = currentTile.getNeighbour(direction);
    
      if (!isBlocked(nextTile)) {
        // Represent the move as an object
        // for easier undos later on
	      Move move(this, direction);
	      move();
	  
	      _moves.push(move);
      }
	
      return !isBlocked(nextTile);
    };
  
    bool canUndo() const {
      return _undoMovesLeft > 0 && !_moves.empty();
    };
  
    bool undoMove() {
      if (!canUndo()) {
        return false;
      }
    
	    _moves.peek().undo();
	    _moves.pop();
      --_undoMovesLeft;
	
      return true;
    };
  
    bool isDark() const {
      return _isDark;
    };

    bool isGameOver() const {
      // The player is blocked and has no undo possibilities left
      return _undoMovesLeft == 0 && isBlocked(getTile(getPlayer().getPosition()));
    };

    bool isWin() const {
      // Ensure that each tile is visited
      for (int i = 0; i < (_width * _height); ++i) {
        if (_tileMap[i].isConnected() && !_tileMap[i].isVisited()) {
          return false;
        }
      }
    
      return true;
    };
 
/*
    // Cannot use named constructor pattern since compiler issues error due to
    // ambiguous call between std::swap and octet::swap on issuing copy constructor
    // for return value for the deque data structure used in member BoundedStack _stack.
    static Dungeon build(const LevelDefinition& definition) {
      Dungeon dungeon(
        definition.getWidth(),
        definition.getHeight(),
        definition.hasProperty(LevelDefinition::Dark),
        definition.getPlayerOrigin()
      );
    
      for (int x = 0; x < definition.getWidth(); ++x) {
        for (int y = 0; y < definition.getHeight(); ++y) {
	      Point2D position(x, y);
          Tile& tile = dungeon.getTile(position);
        
          // Assumption: if tile A connects with tile B, then tile B
          // connects with tile A => undirected graph.
        
          if (definition.isConnected(position, LevelDefinition::North) && (y - 1) >= 0) {
            Tile& northTile = dungeon.getTile(Point2D(x, (y - 1)));
          
            northTile.setNeighbour(&tile, LevelDefinition::South);
            tile.setNeighbour(&northTile, LevelDefinition::North);
          }
        
          if (definition.isConnected(position, LevelDefinition::East) && (x - 1) >= 0) {
            Tile& eastTile = dungeon.getTile(Point2D((x - 1), y));
          
            eastTile.setNeighbour(&tile, LevelDefinition::West);
            tile.setNeighbour(&eastTile, LevelDefinition::East);
          }
        }
      }
	
	    dungeon.getTile(dungeon.getPlayer().getPosition()).setVisited(true);
    
      return dungeon;
    };
*/
  };
  
  // --

  // A Factory pattern impelmentation which hosts hard-coded level definitions
  class LevelDefinitionFactory : public Singleton<LevelDefinitionFactory> {
  private:
    LevelDefinition _level0, _level1, _level2, _level3, _level4, _level5, _level6, _level7;

    // Used for testing purposes in order to test camera zoom-out
    LevelDefinition _levelBig;

    // Representation of a 'null' level
    LevelDefinition _levelVoid;
  
    friend class Singleton<LevelDefinitionFactory>;

    LevelDefinitionFactory() :
      _level0(LevelDefinition::parse("73010006AC0040505043A903A9")),
      _level1(LevelDefinition::parse("550006EEEC7FFFD7FFFD7FFFD3BBB9")),
      _level2(LevelDefinition::parse("550006EEEC5555555555555553BBB9")),
      _level3(LevelDefinition::parse("661006C006C7D007D7FEEFD7FBBFD7D007D390039")),
      _level4(LevelDefinition::parse("A79006EC00006EC3FD00007F907FEAAEFD007FD007FD007FBAABFD06FD00007FC3B900003B9")),
      _level5(LevelDefinition::parse("732006AC06AC507AD053A903A9")),
      _level6(LevelDefinition::parse("732016AC06AC507AD053A903A9")),
      _level7(LevelDefinition::parse("793326AC06AC50505053ED07E90JVQVP000L0L000MVQVS06BD07BC50505053A903A9")),
      _levelBig(LevelDefinition::parse("GG003VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")),
      _levelVoid(LevelDefinition::parse("110000")) {
    };
  
  public:
    ~LevelDefinitionFactory() {
    };

    const LevelDefinition& get(const char* id) const {
      if (strcmp(id, "0") == 0) { return _level0; }
      if (strcmp(id, "1") == 0) { return _level1; }
	    else if (strcmp(id, "2") == 0) { return _level2; }
	    else if (strcmp(id, "3") == 0) { return _level3; }
	    else if (strcmp(id, "4") == 0) { return _level4; }
	    else if (strcmp(id, "5") == 0) { return _level5; }
	    else if (strcmp(id, "6") == 0) { return _level6; }
	    else if (strcmp(id, "7") == 0) { return _level7; }
      else if (strcmp(id, "big") == 0) { return _levelBig; }
	
	    return _levelVoid;
    };

    static int getLevelCount() {
      return 8;
    };
  };
  
  // --

  // Sprite-sheet helper class
  class SpriteSheetInfo {
  private:
    float _uvOneU;
    float _uvOneV;

    float _uvSpriteWidth;
    float _uvSpriteHeight;
    
    float _uvPaddingU;
    float _uvPaddingV;

  public:
    SpriteSheetInfo() :
      _uvOneU(0.f),
      _uvOneV(0.f),
      _uvSpriteWidth(0.f),
      _uvSpriteHeight(0.f),
      _uvPaddingU(0.f),
      _uvPaddingV(0.f) {
    };

    SpriteSheetInfo(
      unsigned int sheetWidthInPixels,
      unsigned int sheetHeightInPixels,
      unsigned int spriteWidthInPixels,
      unsigned int spriteHeightInPixels,
      unsigned int spritePaddingXInPixels = 0,
      unsigned int spritePaddingYInPixels = 0
      ) {
        init(sheetWidthInPixels, sheetHeightInPixels, spriteWidthInPixels, spriteHeightInPixels, spritePaddingXInPixels, spritePaddingYInPixels);
    };

    void init(
      unsigned int sheetWidthInPixels,
      unsigned int sheetHeightInPixels,
      unsigned int spriteWidthInPixels,
      unsigned int spriteHeightInPixels,
      unsigned int spritePaddingXInPixels = 0,
      unsigned int spritePaddingYInPixels = 0
    ) {
      _uvOneU = (1.f / sheetWidthInPixels);
      _uvOneV = (1.f / sheetHeightInPixels);

      _uvSpriteWidth = (spriteWidthInPixels * _uvOneU);
      _uvSpriteHeight = (spriteHeightInPixels * _uvOneV);

      _uvPaddingU = (spritePaddingXInPixels * _uvOneU);
      _uvPaddingV = (spritePaddingYInPixels * _uvOneV);
    };

    ~SpriteSheetInfo() {
    };

    float getTexelWidth() const {
      return _uvOneU;
    };

    float getTexelHeight() const {
      return _uvOneV;
    };

    /**
     * @param spriteX the x-index of the sprite within the sprite sheet
     * @param spriteY the y-index of the sprite within the sprite sheet
     * @param uvs the float[8] uv container of stride 2 which will be populated with the correct values.
     */
    void calculateUVs(unsigned int spriteX, unsigned int spriteY, float uvs[]) const {
      // bottom-left corner
      uvs[0] = (spriteX * _uvSpriteWidth) + ((spriteX + 1) * _uvPaddingU);
      uvs[1] = (spriteY * _uvSpriteHeight) + ((spriteY + 1) *_uvPaddingV);
      
      // bottom-right corner
      uvs[2] = uvs[0] + _uvSpriteWidth;
      uvs[3] = uvs[1];
      
      // top-right corner
      uvs[4] = uvs[2];
      uvs[5] = uvs[1] + _uvSpriteHeight;

      // top-left corner
      uvs[6] = uvs[0];
      uvs[7] = uvs[5];
    };
    
    /**
     * @param spriteIndex the index of the sprite within the sprite sheet
     * @param uvs the float[8] uv container of stride 2 which will be populated with the correct values.
     */
    void calculateUVs(const Point2Di& spriteIndex, float uvs[]) const {
      calculateUVs(spriteIndex.x, spriteIndex.y, uvs);
    };
  };

  // --

  // A replica of texture_shader.h which allows for a further
  // degree of flexibility due to modularising shader source
  // retrieval. This allows for easier extension.
  class TextureShaderEx : public shader {
  private:
    GLint _modelToProjectionIndex;
    GLint _samplerIndex;

  public:
    virtual ~TextureShaderEx() {
    };

    virtual void init() {
      shader::init(getVertexShaderSource(), getFragmentShaderSource());

      // extract the indices of the uniforms to use later
      _modelToProjectionIndex = glGetUniformLocation(program(), "modelToProjection");
      _samplerIndex = glGetUniformLocation(program(), "sampler");
    };

    virtual const char* getVertexShaderSource() {
      return SHADER_STR(
        varying vec2 uv_;

        attribute vec4 pos;
        attribute vec2 uv;

        uniform mat4 modelToProjection;

        void main() { gl_Position = modelToProjection * pos; uv_ = uv; }
      );
    };

    virtual const char* getFragmentShaderSource() {
      return SHADER_STR(
        varying vec2 uv_;
        uniform sampler2D sampler;

        void main() { gl_FragColor = texture2D(sampler, uv_); }
      );
    };

    //using shader::render;

    virtual void render(const mat4t& modelToProjectionTransformer, GLuint textureSampler = 0) {
      shader::render();
      
      glUniform1i(_samplerIndex, textureSampler);
      glUniformMatrix4fv(_modelToProjectionIndex, 1, GL_FALSE, modelToProjectionTransformer.get());
    };
  };

  // --

  // A TextureShaderEx extension which renders a texture in greyscale
  class GreyscaleTextureShader : public TextureShaderEx {
  public:
    virtual const char* getFragmentShaderSource() {
      return SHADER_STR(
        varying vec2 uv_;
        uniform sampler2D sampler;

        // Grayscale via red component replication and vector swizzling
        //void main() { gl_FragColor = texture2D(sampler, uv_).xxxw; }
		  
        // Equivalent to the above but using differenct accessor labels
        //void main() { gl_FragColor = texture2D(sampler, uv_).rrra; }
        
        // Grayscale via colour averaging
        void main() {
          vec4 textureColor = texture2D(sampler, uv_);
          float grey = dot(textureColor.xyz, vec3(1)) / float(3);
          //float grey = dot(textureColor.xyz, vec3(0.2126, 0.7152, 0.0722)) / float(3);
          gl_FragColor = vec4(grey, grey, grey, textureColor.w);
        }
      );
    };
  };

  // --

  // Representation of a 3x3 convolution kernel
  class ConvolutionKernel {
  private:
    float _kernel[3][3];

  public:
    ConvolutionKernel() {
      reset();
    };

    ~ConvolutionKernel() {
    };

    float& operator()(unsigned int x, unsigned int y) {
      return _kernel[x][y];
    };

    float operator()(unsigned int x, unsigned int y) const {
      return _kernel[x][y];
    };

    float* get() {
      return &_kernel[0][0];
    };

    void reset() {
      null(*this);
    };

    // Fills in the target ConvolutionKernel instance with the appropriate values
    // representing the 'null' convolution
    static void null(ConvolutionKernel& target) {
      target._kernel[0][0] = 0; target._kernel[1][0] = 0; target._kernel[2][0] = 0;
      target._kernel[0][1] = 0; target._kernel[1][1] = 1; target._kernel[2][1] = 0;
      target._kernel[0][2] = 0; target._kernel[1][2] = 0; target._kernel[2][2] = 0;
    };

    // Reference: https://developer.apple.com/library/IOs/documentation/Performance/Conceptual/vImage/ConvolutionOperations/ConvolutionOperations.html
    // Reference: http://en.wikipedia.org/wiki/Kernel_(image_processing)
    static void blur(ConvolutionKernel& target) {
      //target._kernel[0][0] = 1; target._kernel[1][0] = 2; target._kernel[2][0] = 1;
      //target._kernel[0][1] = 2; target._kernel[1][1] = 4; target._kernel[2][1] = 2;
      //target._kernel[0][2] = 1; target._kernel[1][2] = 2; target._kernel[2][2] = 1;

      target._kernel[0][0] = 1; target._kernel[1][0] = 1; target._kernel[2][0] = 1;
      target._kernel[0][1] = 1; target._kernel[1][1] = 1; target._kernel[2][1] = 1;
      target._kernel[0][2] = 1; target._kernel[1][2] = 1; target._kernel[2][2] = 1;
    };

    static void edgeDetect(ConvolutionKernel& target) {
      target._kernel[0][0] = +0; target._kernel[1][0] = -1; target._kernel[2][0] = +0;
      target._kernel[0][1] = -1; target._kernel[1][1] = +4; target._kernel[2][1] = -1;
      target._kernel[0][2] = +0; target._kernel[1][2] = -1; target._kernel[2][2] = +0;
    };

    // Laplace
    // Reference: http://homepages.inf.ed.ac.uk/rbf/HIPR2/log.htm
  };

  // --

  // A TextureShaderEx extension which allows convolution filters
  // to be applied to a texture.
  //
  // WARNING Experimental. Not sure if it is correct.
  //
  class ConvolutionTextureShader : public TextureShaderEx {
  private:
    ConvolutionKernel _kernel;
    GLint _kernelLocation;

    float _texelWidth;
    GLint _texelWidthLocation;

    float _texelHeight;
    GLint _texelHeightLocation;

  public:
    ConvolutionKernel& getKernel() {
      return _kernel;
    };

    const ConvolutionKernel& getKernel() const {
      return _kernel;
    };

    void setKernel(const ConvolutionKernel& kernel) {
      _kernel = kernel;
    };

    void setTexelWidth(float texelWidth) {
      _texelWidth = texelWidth;
    };
    
    void setTexelHeight(float texelHeight) {
      _texelHeight = texelHeight;
    };

    virtual void init() {
      TextureShaderEx::init();

      // Once the shader program is linked, extract the locations of uniforms
      // specified in the vertex/fragment glsl scripts for later reference
      _texelWidthLocation = glGetUniformLocation(program(), "texelWidth");
      _texelHeightLocation = glGetUniformLocation(program(), "texelHeight");
      _kernelLocation = glGetUniformLocation(program(), "convolutionKernel");
    };

    
    // Reference: http://stackoverflow.com/questions/13872544/gcc-stringification-and-inline-glsl
    // Version attribute allows for storage precision (e.g. mediump) specification
    #define GLSL(version, shader) "#version " #version "\n" #shader

    // Reference: http://stackoverflow.com/questions/12469990/simple-glsl-convolution-shader-is-atrociously-slow
    virtual const char* getVertexShaderSource() {
      return GLSL(100,
        attribute vec4 pos;
        attribute vec2 uv;

        uniform float texelWidth; 
        uniform float texelHeight; 

        // NOTE texture uv coordinates are calculated in vertex shader since
        //      calculating these in the fragment shader would lead to
        //      inefficient processing
        // Reference: Be Aware of Dynamic Texture Lookups - https://developer.apple.com/library/ios/documentation/3DDrawing/Conceptual/OpenGLES_ProgrammingGuide/BestPracticesforShaders/BestPracticesforShaders.html
        
        varying vec2 uv_;
        varying vec2 leftTextureCoordinate;
        varying vec2 rightTextureCoordinate;

        varying vec2 topTextureCoordinate;
        varying vec2 topLeftTextureCoordinate;
        varying vec2 topRightTextureCoordinate;

        varying vec2 bottomTextureCoordinate;
        varying vec2 bottomLeftTextureCoordinate;
        varying vec2 bottomRightTextureCoordinate;

        uniform mat4 modelToProjection;

        void main() {
          gl_Position = modelToProjection * pos;
          
          vec2 widthStep = vec2(texelWidth, 0.0);
          vec2 heightStep = vec2(0.0, texelHeight);
          vec2 widthHeightStep = vec2(texelWidth, texelHeight);
          vec2 widthNegativeHeightStep = vec2(texelWidth, -texelHeight);

          uv_ = uv;
          leftTextureCoordinate = uv - widthStep;
          rightTextureCoordinate = uv + widthStep;

          topTextureCoordinate = uv - heightStep;
          topLeftTextureCoordinate = uv - widthHeightStep;
          topRightTextureCoordinate = uv + widthNegativeHeightStep;

          bottomTextureCoordinate = uv + heightStep;
          bottomLeftTextureCoordinate = uv - widthNegativeHeightStep;
          bottomRightTextureCoordinate = uv + widthHeightStep;
        }
      );
    };

    virtual const char* getFragmentShaderSource() {
      return GLSL(100,
        uniform sampler2D sampler;
        uniform mat3 convolutionKernel;

        varying vec2 uv_;
        varying vec2 leftTextureCoordinate;
        varying vec2 rightTextureCoordinate;

        varying vec2 topTextureCoordinate;
        varying vec2 topLeftTextureCoordinate;
        varying vec2 topRightTextureCoordinate;

        varying vec2 bottomTextureCoordinate;
        varying vec2 bottomLeftTextureCoordinate;
        varying vec2 bottomRightTextureCoordinate;

        void main() {
          vec4 bottomColor = texture2D(sampler, bottomTextureCoordinate);
          vec4 bottomLeftColor = texture2D(sampler, bottomLeftTextureCoordinate);
          vec4 bottomRightColor = texture2D(sampler, bottomRightTextureCoordinate);
          vec4 centerColor = texture2D(sampler, uv_);
          vec4 leftColor = texture2D(sampler, leftTextureCoordinate);
          vec4 rightColor = texture2D(sampler, rightTextureCoordinate);
          vec4 topColor = texture2D(sampler, topTextureCoordinate);
          vec4 topRightColor = texture2D(sampler, topRightTextureCoordinate);
          vec4 topLeftColor = texture2D(sampler, topLeftTextureCoordinate);

          vec4 resultColor = topLeftColor * convolutionKernel[0][0] + topColor * convolutionKernel[0][1] + topRightColor * convolutionKernel[0][2];
          resultColor += leftColor * convolutionKernel[1][0] + centerColor * convolutionKernel[1][1] + rightColor * convolutionKernel[1][2];
          resultColor += bottomLeftColor * convolutionKernel[2][0] + bottomColor * convolutionKernel[2][1] + bottomRightColor * convolutionKernel[2][2];

          //vec4 resultColor = topLeftColor + topColor + topRightColor;
          //resultColor += leftColor + centerColor + rightColor;
          //resultColor += bottomLeftColor + bottomColor + bottomRightColor;

          gl_FragColor = resultColor;
        }
      );
    };

    #undef GLSL

    virtual void render(const mat4t& modelToProjectionTransformer, GLuint textureSampler = 0) {
      TextureShaderEx::render(modelToProjectionTransformer, textureSampler);
      
      // Instruct OpenGL about the locations/values of particular uniforms which are
      // later copied to VRAM/referenced from general memory
      glUniformMatrix3fv(_kernelLocation, 1, GL_FALSE, _kernel.get());
      glUniform1f(_texelWidthLocation, _texelWidth);
      glUniform1f(_texelHeightLocation, _texelHeight);
    };
  };

  // --

  // Adapted from invaderers_app.h
  class Sprite {
    
    // where is our sprite (overkill for a ping game!)
    mat4t modelToWorld;

    // half the width of the sprite
    float halfWidth;

    // half the height of the sprite
    float halfHeight;

    // what texture is on our sprite
    GLuint texture;

    // UV values for the sprite
    // i.e. the x and y coordinates (0.0, 1.0) of the sprite in
    //      texture space
    // Reference: http://en.wikipedia.org/wiki/UV_mapping
    float _uvs[8];

  public:
    Sprite() :
      halfWidth(0.f),
      halfHeight(0.f),
      texture(0) {
        loadDefaultUVs();
    };

    static float getDefaultWidthHeight() {
      return .4f;
    };

    void init(int _texture, float x = 0.f, float y = 0.f, float w = getDefaultWidthHeight(), float h = getDefaultWidthHeight()) {
      position(x, y);
      halfWidth = w * 0.5f;
      halfHeight = h * 0.5f;
      texture = _texture;
    };
    
    float* getUVs() {
      return _uvs;
    };
    
    void loadDefaultUVs() {
      _uvs[0] = 0;
      _uvs[1] = 0;

      _uvs[2] = 1;
      _uvs[3] = 0;

      _uvs[4] = 1;
      _uvs[5] = 1;
      
      _uvs[6] = 0;
      _uvs[7] = 1;
    };

    void render(TextureShaderEx &shader, mat4t &cameraToWorld) {
      // invisible sprite... used for gameplay.
      if (!texture) return;

      // build a projection matrix: model -> world -> camera -> projection
      // the projection space is the cube -1 <= x/w, y/w, z/w <= 1
      mat4t modelToProjection = mat4t::build_projection_matrix(modelToWorld, cameraToWorld);

      // set up opengl to draw textured triangles using sampler 0 (GL_TEXTURE0)
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture);

      // use "old skool" rendering
      // NOTE When our sprite sheet has no padding between sprites, GL_LINEAR would lead to sprite sheet bleeding
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      shader.render(modelToProjection, 0);

      // this is an array of the positions of the corners of the sprite in 3D
      // a straight "float" here means this array is being generated here at runtime.
      float vertices[] = {
        -halfWidth, -halfHeight, 0,
         halfWidth, -halfHeight, 0,
         halfWidth,  halfHeight, 0,
        -halfWidth,  halfHeight, 0,
      };

      // attribute_pos (=0) is position of each corner
      // each corner has 3 floats (x, y, z)
      // there is no gap between the 3 floats and hence the stride is 3*sizeof(float)
      glVertexAttribPointer(attribute_pos, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)vertices);
      glEnableVertexAttribArray(attribute_pos);
    
      // attribute_uv is position in the texture of each corner
      // each corner (vertex) has 2 floats (x, y)
      // there is no gap between the 2 floats and hence the stride is 2*sizeof(float)
      glVertexAttribPointer(attribute_uv, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)_uvs);
      glEnableVertexAttribArray(attribute_uv);
    
      // finally, draw the sprite (4 vertices)
      glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    };

    // move the object
    void translate(float x, float y) {
      modelToWorld.translate(x, y, 0);
    };
    
    // position the object relative to another.
    void set_relative(Sprite &rhs, float x, float y) {
      modelToWorld = rhs.modelToWorld;
      modelToWorld.translate(x, y, 0);
    };
    
    // absolute positioning of the object.
    void position(float x, float y) {
      modelToWorld.loadIdentity();
      modelToWorld.translate(x, y, 0);
    };
  };
  
  // --

  // A utility class which represents a font using
  // individual sprites within a sprite sheet
  class SpriteFont {
  private:
    SpriteSheetInfo _sheet;
    GLuint _texture;

    // Using std::map rather than the faster octet::dictionary since
    // octet::dictionary is tailor-made for char* key types.
    std::map<char, Point2Di> _characterSpriteIndex;

  public:
    // Implementation of the Iterator pattern which
    // eventually yields a Sprite representation per
    // character for the requested text
    class Iterator {
    private:
      const SpriteFont* _outerType;
      const char* _text;

      // Index of current character
      unsigned int _index;

      // Default position and size values
      float _x;
      float _y;
      float _width;
      float _height;

      Sprite _characterSprite;

      Iterator(
        const SpriteFont* outerType,
        const char* text,
        float x = 0.f,
        float y = 0.f,
        float width = 0.f,
        float height = 0.f
      ) :
        _outerType(outerType),
        _text(text),
        _index(0),
        _x(x), 
        _y(y), 
        _width(width), 
        _height(height) {
      };

      friend class SpriteFont;

    public:
      ~Iterator() {
      };
      
      bool hasNext() {
        return _text[_index] != '\0';
      };

      Iterator& next() {
        ++_index;
        return *this;
      };

      Iterator& operator++() {
        return next();
      };
      
      Iterator& reset() {
        _index = 0;
        return *this;
      };

      Sprite& get() {
        // NOTE
        // - We do not handle new lines, spaces, tabs and similar special characters.
        // - Assuming character spacing is equivalent to sprite width.
        return get(_x + (_index * _width), _y, _width, _height);
      };
      
      Sprite& operator*() {
        return get();
      };

      Sprite* operator->() {
        return &get();
      };

      Sprite& get(float x, float y, float width, float height) {
        // Modify the singular Sprite instance for the current character
        _characterSprite.init(_outerType->_texture, x, y, width, height);
        
        // Modify the UV values for current character
        _outerType->_sheet.calculateUVs(
          // Retrieve the sprite positions for the mapped character
          _outerType->_characterSpriteIndex.at(_text[_index]),
          _characterSprite.getUVs()
        );
        
        return _characterSprite;
      };
    };

    SpriteFont() {
    };

    SpriteFont(GLuint texture, const SpriteSheetInfo& sheetInfo) :
      _sheet(sheetInfo),
      _texture(texture) {
    };

    ~SpriteFont() {
    };

    void init(GLuint texture, const SpriteSheetInfo& sheetInfo) {
      _sheet = sheetInfo;
      _texture = texture;
    };

    // Associated the character with a particular X and Y location of a 
    // sprite within the sprite sheet
    void define(char character, const Point2Di& spriteIndex) {
      _characterSpriteIndex[character] = spriteIndex;
    };

    // Associated the range of characters with a particular X and Y location of a 
    // sprite within the sprite sheet. ASCII codes are used to define the range.
    void define(char startCharacter, char endCharacter, const Point2Di& spriteIndex) {
      float uvsBuffer[8];
      Point2Di tempSpriteIndex = spriteIndex;
      
      while (startCharacter <= endCharacter) {
        _sheet.calculateUVs(tempSpriteIndex, uvsBuffer);

        // Ensure that we are still within (width) bounds of the SpriteSheet
        if (!(uvsBuffer[0] < 1.0)) {
          // If not, flow on to the line below
          tempSpriteIndex.x = 0;
          // NOTE positive V points upwards
          --tempSpriteIndex.y;
        }

        define(startCharacter, tempSpriteIndex);
        
        // Infer inbetween characters based on ASCII code table
        ++startCharacter;

        ++tempSpriteIndex.x;
      };
    };

    // Provides a Sprite iterator which represents the requested word
    Iterator get(const char* text, float x = 0.f, float y = 0.f, float width = 0.f, float height = 0.f) const {
      return Iterator(this, text, x, y, width, height);
    };
  };

  // --

  // Utility class which manages input.
  // Used to delay key-press.
  class InputManager {
  private:
    app* _app;

    // The key delay amount per poll request
    unsigned int _keyDelay;

    // Delay accumulator for processing key presses
    unsigned int _delay;

    // Non-copyable entity
    InputManager(const InputManager&);
    InputManager& operator=(const InputManager&);

  public:
    typedef unsigned int keycode;

    InputManager(app* app = NULL, unsigned int keyDelay = 5) :
      _app(app),
      _keyDelay(keyDelay),
      _delay(0) {
    };

    ~InputManager() {
    };

    void attach(app* app) {
      _app = app;
      _delay = 0;
    };

    void setKeyDelay(unsigned int keyDelay) {
      _keyDelay = keyDelay;
      _delay = 0;
    };

    bool isKeyDown(keycode key) const {
      return _delay == 0 && _app != NULL && _app->is_key_down(key);
    };

    bool processKey(keycode key) {
      if (isKeyDown(key)) {
        _delay = _keyDelay;
        return true;
      };

      return false;
    };

    // Check for key-presses
    void poll() {
      _delay = (_delay == 0 ? 0 : _delay - 1);
    };
  };

  // --

  class dungeon_app : public app {
    // Matrix to transform points in our camera space to the world.
    // This lets us move our camera
    mat4t cameraToWorld;

    struct {
      // shader to draw a textured triangle
      TextureShaderEx textureShader;
      
      // shader to draw a textured triangle in greyscale
      GreyscaleTextureShader greyscaleShader;

      // shader to draw a texture trianlge by applying convolution filters
      ConvolutionTextureShader convolutionShader;
    } _shaders;
    
    // The input manager instance
    InputManager _inputManager;

    // The current level.
    // Negative values implies user requested a specific level.
    int _level;

    // The current dungeon instance
    Dungeon _dungeon;

    // Forward Declaration
    class GameState;
    // The current game state
    GameState* _state;

    // The sprite representations of the dungeon elements
    struct {
      Sprite tile;
      Sprite visitedTile;
      Sprite verticalWall;
      Sprite horizontalWall;

      Sprite player;

      Sprite goldRing;
      Sprite silverRing;
      Sprite bronzeRing;

      SpriteFont font;
    } _sprites;

    // Audio instantations
    struct {
      ALuint bgm;
      ALuint source[1];
    } _audio;

    // --

    // A State pattern abstract-base class
    class GameState {
    protected:
      GameState() {
      };

      virtual void handleInput(dungeon_app* app) {
        app->_inputManager.poll();
      };

      virtual void simulate(dungeon_app* app) {
        handleInput(app);
      };

      virtual void draw(dungeon_app* app) {
      };

    public:
      virtual ~GameState() = 0 {
      };
      
      // Issued once the state machine switches to this state
      virtual void onChange(dungeon_app* app) {
        app->resetCamera(2.5f);
      };

      // The execution loop of this state
      virtual void operator()(dungeon_app* app, int x, int y, int width, int height) {
        simulate(app);
        if (app->getState() != this) {
          return;
        }

        app->clearWindow(x, y, width, height);
        draw(app);
      };
    };

    // Handles initialisation
    class StartState : public GameState, public Singleton<StartState> {
    private:
      void playBGM(dungeon_app* app) {
        ALuint source = app->_audio.source[0];
        
        alSourcei(source, AL_BUFFER, app->_audio.bgm);
        alSourcei(source, AL_LOOPING, 1);
        
        alSourcePlay(source);
      };

    public:
      void onChange(dungeon_app* app) {
        playBGM(app);
      };

      void operator()(dungeon_app* app, int x, int y, int width, int height) {
        // If the user has requested a particular level from the command-line,
        // immediatly switch to the GamePlayState
        if (app->_level < 0) {
          app->setState(&GamePlayState::getInstance());
        } else {
        // Else show the title screen
          app->setState(&TitleScreenState::getInstance());
        }
      };
    };

    class EndState : public GameState, public Singleton<EndState> {
    private:
      void endBGM(dungeon_app* app) {
        ALuint source = app->_audio.source[0];
        alSourceStop(source);
      };

      void draw(dungeon_app* app) {
        app->drawText("Thank you for playing!", Point2Df(-1.55f, 0.f));
      };

    public:
      void onChange(dungeon_app* app) {
        GameState::onChange(app);
        endBGM(app);
      };

      void operator()(dungeon_app* app, int x, int y, int width, int height) {
        app->clearWindow(x, y, width, height);
        draw(app);
      };
    };

    class TitleScreenState : public GameState, public Singleton<TitleScreenState> {
    private:
      void handleInput(dungeon_app* app) {
        GameState::handleInput(app);

        if (app->_inputManager.processKey(key_esc)) {
          app->setState(&EndState::getInstance());
        } else if (app->_inputManager.processKey(key_space)) {
          app->setState(&LevelIntroState::getInstance());
        }
      };

      void draw(dungeon_app* app) {
        GameState::draw(app);

        Point2Df position(-1.3f, 1.f);
        app->drawText("Untitled Maze Game", position);

        position.x = -1.6f; position.y = -1.f;
        app->drawText("Press 'SPACE' to start.", position);
      };

    public:
      void onChange(dungeon_app* app) {
        GameState::onChange(app);
        if (app->_level != 0) {
          app->setLevel(0);
        }
      };
    };

    class LevelIntroState : public GameState, public Singleton<LevelIntroState> {
    private:
      void handleInput(dungeon_app* app) {
        app->_inputManager.poll();

        if (app->_inputManager.processKey(key_esc)) {
          app->setState(&TitleScreenState::getInstance());
        } else if (app->_inputManager.processKey(key_space)) {
          app->setState(&GamePlayState::getInstance());
        }
      };

      void draw(dungeon_app* app) {
        static char buffer[10];
        sprintf(buffer, "Level %d", app->_level);

        Point2Df position(-0.42f, 1.f);
        app->drawText(buffer, position);

        position.x = -1.8f; position.y = -1.f;
        app->drawText("Press 'SPACE' to continue.", position);
      };
    };

    class GamePlayState : public GameState, public Singleton<GamePlayState> {
    private:
      mat4t _tempCamera;
      // Pointer to current texture shader in use
      TextureShaderEx* _shader;

      friend class Singleton<GamePlayState>;

      GamePlayState() :
        _shader(NULL) {
      };

      typedef void (*kernelManipulator)(ConvolutionKernel&);
      void setConvolutionShader(dungeon_app* app, kernelManipulator manipulator) {
        if (_shader == &(app->_shaders.textureShader)) {
          // Specify the values of the convolution kernel directly on the
          // convolution kernel instance member of the convolution shader
          manipulator(app->_shaders.convolutionShader.getKernel());
          _shader = &(app->_shaders.convolutionShader);
        } else {
          app->_shaders.convolutionShader.getKernel().reset();
          _shader = &(app->_shaders.textureShader);
        }
      };

      void handleInput(dungeon_app* app) {
        app->_inputManager.poll();

        if (app->_inputManager.processKey(key_esc)) {
          if (app->_level < 0) {
            app->setState(&EndState::getInstance());
          } else {
            // Reset current dungeon instance
            app->setLevel(app->_level);
            app->setState(&LevelIntroState::getInstance());
          }
        } else if (app->_inputManager.processKey(key_up)) {
          app->movePlayer(LevelDefinition::North);
        } else if (app->_inputManager.processKey(key_right)) {
          app->movePlayer(LevelDefinition::East);
        } else if (app->_inputManager.processKey(key_down)) {
          app->movePlayer(LevelDefinition::South);
        } else if (app->_inputManager.processKey(key_left)) {
          app->movePlayer(LevelDefinition::West);
        } else if (app->_inputManager.processKey(key_backspace)) {
          app->undoMove();
        } else if (app->_inputManager.processKey(key_f1)) {
          _shader = (_shader == &(app->_shaders.textureShader) ? &(app->_shaders.greyscaleShader) : &(app->_shaders.textureShader));
        } else if (app->_inputManager.processKey(key_f2)) {
          setConvolutionShader(app, &ConvolutionKernel::blur);
        } else if (app->_inputManager.processKey(key_f3)) {
          setConvolutionShader(app, &ConvolutionKernel::edgeDetect);
        }
      };

      // called every frame to move things
      void simulate(dungeon_app* app) {
        if (app->_dungeon.isWin()) {
          
          app->setState(&WinScreenState::getInstance());
          return;

        } else if (app->_dungeon.isGameOver()) {

          app->setState(&LoseScreenState::getInstance());
          return;

        } else {

          handleInput(app);
          if (app->getState() != this) {
            return;
          }

        }
      };

      void drawHUD(dungeon_app* app) {
        static char buffer[15];
        const Dungeon& dungeon = app->getConstDungeon();

        // Zoom out camera accordingly for HUD
        // Keep a temporary copy for later restoration
        _tempCamera = app->cameraToWorld;
        app->cameraToWorld.loadIdentity();
        app->cameraToWorld.translate(0.f, 0.f, 3.f);

        sprintf(buffer, "Moves '%d'", dungeon.getMoveCount());
        app->drawText(buffer, Point2Df(-2.5f, 2.5f));

        sprintf(buffer, "Undos '%d'", dungeon.getAvailableUndoCount());
        app->drawText(buffer, Point2Df(1.3f, 2.5f));

        app->cameraToWorld = _tempCamera;
      };

      void drawPlayer(dungeon_app* app, const Player& player) {
        app->setSpritePosition(app->_sprites.player, player.getPosition());
        app->_sprites.player.render(*_shader, app->cameraToWorld);
      };

      void drawHorizontalWall(dungeon_app* app, const Point2Di& position, LevelDefinition::Direction direction) {
        app->setSpritePosition(app->_sprites.horizontalWall, position);
        app->_sprites.horizontalWall.translate(0, (direction == LevelDefinition::North ? +1 : 0) * Sprite::getDefaultWidthHeight() - getWallOffset());

        app->_sprites.horizontalWall.render(*_shader, app->cameraToWorld);
      };

      void drawVerticalWall(dungeon_app* app, const Point2Di& position, LevelDefinition::Direction direction) {
        app->setSpritePosition(app->_sprites.verticalWall, position);
        app->_sprites.verticalWall.translate((direction == LevelDefinition::West ? -1 : 0) * Sprite::getDefaultWidthHeight() + getWallOffset(), 0);

        app->_sprites.verticalWall.render(*_shader, app->cameraToWorld);
      };

      void drawWalls(dungeon_app* app, const Tile& tile, const Point2Di& tilePosition) {
        if (!tile.isConnected()) {
          return;
        }

        if (tile.getNeighbour(LevelDefinition::West) == NULL) {
          drawVerticalWall(app, tilePosition, LevelDefinition::West);
        }
        if (tile.getNeighbour(LevelDefinition::East) == NULL) {
          drawVerticalWall(app, tilePosition, LevelDefinition::East);
        }
        if (tile.getNeighbour(LevelDefinition::North) == NULL) {
          drawHorizontalWall(app, tilePosition, LevelDefinition::North);
        }
        if (tile.getNeighbour(LevelDefinition::South) == NULL) {
          drawHorizontalWall(app, tilePosition, LevelDefinition::South);
        }
      };

      void drawTile(dungeon_app* app, const Tile& tile, const Point2Di& tilePosition) {
        Sprite* tileSprite = NULL;
        bool isPlayerOnTile = app->getConstDungeon().getPlayer().getPosition() == tilePosition;
      
        // Choose appropriate tile
        if (tile.isVisited() && !tile.isRetraversable() && !isPlayerOnTile) {
          tileSprite = &app->_sprites.visitedTile;
        } else if (tile.isConnected()) {
          tileSprite = &app->_sprites.tile;
        }

        // Render tile
        if (tileSprite != NULL) {
          app->setSpritePosition(*tileSprite, tilePosition);
          tileSprite->render(*_shader, app->cameraToWorld);
        }

        // Render ring
        if (tile.isConnected() && !tile.isVisited() && !isPlayerOnTile && tile.getUserData() != NULL) {
          Sprite* ring = (Sprite*) tile.getUserData();

          app->setSpritePosition(*ring, tilePosition);
          ring->render(*_shader, app->cameraToWorld);
        }
      };

      bool shouldDrawTile(const Dungeon& dungeon, const Tile& tile, const Point2Di tilePosition) {
        const Point2Di& playerPosition = dungeon.getPlayer().getPosition();

        return !dungeon.isDark() || tile.isVisited() || 
                (dungeon.isDark() && (
                  (::abs(playerPosition.x - tilePosition.x) < 2 && playerPosition.y == tilePosition.y) ||
                  (::abs(playerPosition.y - tilePosition.y) < 2 && playerPosition.x == tilePosition.x)
                ));
      };

      void draw(dungeon_app* app) {
        const Dungeon& dungeon = app->getConstDungeon();

        Point2Di point;
        for (point.y = 0; point.y < dungeon.getHeight(); ++point.y) {
          for (point.x = 0; point.x < dungeon.getWidth(); ++point.x) {
            const Tile& tile = dungeon.getTile(point);

            if (shouldDrawTile(dungeon, tile, point)) {
              drawTile(app, tile, point);
              drawWalls(app, tile, point);
            }
          }
        }

        drawPlayer(app, dungeon.getPlayer());
        drawHUD(app);
      };

    public:
      void onChange(dungeon_app* app) {
        app->resetCamera(app->getCameraScale());

        if (_shader == NULL) {
          _shader = &(app->_shaders.textureShader);
        };
      };
    };

    class WinScreenState : public GameState, public Singleton<WinScreenState> {
    private:
      void handleInput(dungeon_app* app) {
        app->_inputManager.poll();

        if (app->_inputManager.processKey(key_space)) {
          if (!(app->_level < 0) && ++app->_level < LevelDefinitionFactory::getLevelCount()) {
            app->setLevel(app->_level);
            app->setState(&LevelIntroState::getInstance());
          } else {
            app->setState(&TitleScreenState::getInstance());
          }
        } else if (app->_inputManager.processKey(key_esc)) {
          app->setState(&TitleScreenState::getInstance());
        }
      };

      void draw(dungeon_app* app) {
        char buffer[30];
        sprintf(buffer, "You finished in %d moves.", app->_dungeon.getMoveCount());

        Point2Df position(-1.75f, 1.f);
        app->drawText("Congratulations! You Won!", position);

        position.x = -1.75f; position.y = 0.f;
        app->drawText(buffer, position);

        position.x = -1.8f; position.y = -1.f;
        app->drawText("Press 'SPACE' to continue.", position);
      };
    };
    
    class LoseScreenState : public GameState, public Singleton<LoseScreenState> {
    private:
      void handleInput(dungeon_app* app) {
        app->_inputManager.poll();

        if (app->_inputManager.processKey(key_space)) {
          if (!(app->_level < 0)) {
            app->setLevel(app->_level);
            app->setState(&LevelIntroState::getInstance());
          } else {
            app->setState(&TitleScreenState::getInstance());
          }
        } else if (app->_inputManager.processKey(key_esc)) {
          app->setState(&TitleScreenState::getInstance());
        }
      };

      void draw(dungeon_app* app) {
        Point2Df position(-1.55f, 1.f);
        app->drawText("Tough Luck. Game Over.", position);

        position.x = -1.8f; position.y = -1.f;
        app->drawText("Press 'SPACE' to try again.", position);
      };
    };

    // --

    enum Status { error = -2, user_requested_level = -1, general_operation = 0 };

    // Parses the command-line and returns the starting LevelDefinition and a status
    // report in the status variable
    static LevelDefinition parse(int argc, char* argv[], int& status) {
      status = user_requested_level;

      int i = 1;
      while (i < argc) {
        if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--level") == 0 && argc >= i + 1) {

          LevelDefinition definition = LevelDefinition::parse(argv[++i]);
          if (definition.isValid()) {
            printf("Loading custom dungeon: %s\n", argv[i]);
            return definition;
          } else {
            printf("Invalid dungeon definition: %s\n", argv[i]);
            status = error;

            return LevelDefinitionFactory::getInstance().get("null");
          }

	      } else if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--dungeon") == 0 && argc >= i + 1) {

          printf("Loading preset dungeon: %s\n", argv[i + 1]);
	        return LevelDefinitionFactory::getInstance().get(argv[++i]);

	      }
	
	      ++i;
      }
  
      printf("Loading preset dungeon: 0\n");
      status = general_operation;

      return LevelDefinitionFactory::getInstance().get("0");
    };

    Dungeon& getDungeon() {
      return _dungeon;
    };

    // const getter
    const Dungeon& getConstDungeon() const {
      return _dungeon;
    };

    GameState* getState() {
      return _state;
    };

    void setState(GameState* state) {
      _state = state;
      
      if (_state != NULL) {
        _state->onChange(this);
      }
    };

    void initialiseShaders() {
      _shaders.textureShader.init();
      _shaders.greyscaleShader.init();
      _shaders.convolutionShader.init();
    };
    
    void initialiseAudio() {
      // Courtesy of http://open.commonly.cc/
      _audio.bgm = resources::get_sound_handle(AL_FORMAT_MONO16, "assets/dungeon/bgm.wav");
      alGenSources(1, _audio.source);

      // Assume listener is at center
      alListener3f(AL_POSITION, 0, 0, 0);
    };

    void initialiseSprite(Sprite& sprite, GLuint texture, const SpriteSheetInfo& info, int spriteX, int spriteY) {
      sprite.init(texture);
      // Calculate and store the UV indices directly in the sprite instance
      info.calculateUVs(spriteX, spriteY, sprite.getUVs());
    };

    void loadSprites() {
      octet::random rand((unsigned int) time(NULL));

      // Courtesy of http://www.opengameart.com
      GLuint dungeonSpriteSheet = resources::get_texture_handle(GL_RGBA, "assets/dungeon/dungeon_padded.gif");
      GLuint fontSpriteSheet = resources::get_texture_handle(GL_RGBA, "assets/dungeon/font.gif");

      SpriteSheetInfo dungeonSpriteSheetInfo(166, 67, 32, 32, 1, 1);

      // Prepare convolution shader to be specifically applied to the dungeon sprite sheet
      _shaders.convolutionShader.setTexelHeight(dungeonSpriteSheetInfo.getTexelHeight());
      _shaders.convolutionShader.setTexelHeight(dungeonSpriteSheetInfo.getTexelWidth());

      initialiseSprite(_sprites.tile, dungeonSpriteSheet, dungeonSpriteSheetInfo, 0, 1);
      initialiseSprite(_sprites.visitedTile, dungeonSpriteSheet, dungeonSpriteSheetInfo, 1, 1);
      
      initialiseSprite(_sprites.verticalWall, dungeonSpriteSheet, dungeonSpriteSheetInfo, 2, 1);
      initialiseSprite(_sprites.horizontalWall, dungeonSpriteSheet, dungeonSpriteSheetInfo, 3, 1);

      // 50:50 chance of using sprite (4,0) or sprite (3,0)
      initialiseSprite(_sprites.player, dungeonSpriteSheet, dungeonSpriteSheetInfo, (rand.get(0.f, 1.0f) > 0.5f ? 3 : 4), 0);

      initialiseSprite(_sprites.goldRing, dungeonSpriteSheet, dungeonSpriteSheetInfo, 0, 0);
      initialiseSprite(_sprites.silverRing, dungeonSpriteSheet, dungeonSpriteSheetInfo, 1, 0);
      initialiseSprite(_sprites.bronzeRing, dungeonSpriteSheet, dungeonSpriteSheetInfo, 2, 0);

      // Initializing font
      _sprites.font.init(fontSpriteSheet, SpriteSheetInfo(522, 17, 9, 17));
      _sprites.font.define('a', 'z', Point2Di(32, 0));
      _sprites.font.define('A', 'Z', Point2Di(32, 0));
      _sprites.font.define('0', '9', Point2Di(15, 0));
      _sprites.font.define(' ', Point2Di(2, 0));
      _sprites.font.define('"', Point2Di(1, 0));
      _sprites.font.define('\'', Point2Di(1, 0));
      _sprites.font.define('.', Point2Di(13, 0));
      _sprites.font.define('!', Point2Di(0, 0));
      _sprites.font.define('?', Point2Di(30, 0));

      initialiseRings();
    };

    void initialiseRings() {
      octet::random rand((unsigned int) time(NULL));
      
      // Assign ring textures to Tiles
      Point2Di point;
      for (point.y = 0; point.y < _dungeon.getHeight(); ++point.y) {
        for (point.x = 0; point.x < _dungeon.getWidth(); ++point.x) {
          const Tile& tile = getConstDungeon().getTile(point);
          if (!tile.isVisited() && tile.isConnected()) {
            int ring = rand.get(0, 7);
            // Assign random ring to tile
            tile.setUserData((void*) (ring > 5 ? &_sprites.goldRing : (ring > 3 ? &_sprites.silverRing : &_sprites.bronzeRing)));
          }
        }
      }
    };
    
    void setLevel(int level) {
      static char buffer[5];
      // Convert integer into string in order to make use of the
      // LevelDefinitionFactory
      sprintf(buffer, "%d", level);

      _level = level;
      _dungeon.reset(LevelDefinitionFactory::getInstance().get(buffer));

      initialiseRings();
    };

    void movePlayer(LevelDefinition::Direction direction) {
      _dungeon.movePlayer(direction);
    };

    void undoMove() {
      _dungeon.undoMove();
    };
    
    static float getWallOffset() {
      return 0.02f;
    };

    void clearWindow(int x, int y, int w, int h) {
      // set a viewport - includes whole window area
      glViewport(x, y, w, h);

      // clear the background to black
      glClearColor(0, 0, 0, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // don't allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glDisable(GL_DEPTH_TEST);

      // allow alpha blend (transparency when alpha channel is 0)
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    };

    void setSpritePosition(
      Sprite& sprite,
      const Point2Di& position,
      const Point2Df& screenOffset,
      float width = Sprite::getDefaultWidthHeight(),
      float height = Sprite::getDefaultWidthHeight()
    ) {
      float spritePositionX = screenOffset.x + (position.x * width);
      float spritePositionY = screenOffset.y + -(position.y * height);

      sprite.position(spritePositionX, spritePositionY);
    };

    void setSpritePosition(
      Sprite& sprite,
      const Point2Di& position,
      float width = Sprite::getDefaultWidthHeight(),
      float height = Sprite::getDefaultWidthHeight()
    ) {
      setSpritePosition(sprite, position, getScreenOffset(), width, height);
    };
    
    void drawText(const char* const text, const Point2Df& screenPosition) {
      drawText(text, screenPosition, _shaders.textureShader);
    };

    void drawText(const char* const text, const Point2Df& screenPosition, TextureShaderEx& shader) {
      SpriteFont::Iterator textIterator =
        _sprites.font.get(text, screenPosition.x, screenPosition.y, .15f, .2f);

      // Render each and every character sprite next to each other
      // as provided by the iterator instance
      for (; textIterator.hasNext(); ++textIterator) {
        textIterator->render(shader, cameraToWorld);
      }
    };

    Point2Df getScreenOffset() const {
      return Point2Df(
        -((getConstDungeon().getWidth() - 1) * Sprite::getDefaultWidthHeight() * 0.5f), 
        +((getConstDungeon().getHeight() - 1) * Sprite::getDefaultWidthHeight() * 0.5f)
      );
    };

    void resetCamera(float zoomOut = 3.f) {
      // set up the matrices with a camera Z units from the origin
      cameraToWorld.loadIdentity();
      cameraToWorld.translate(0, 0, zoomOut);
    };

    float getCameraScale() const {
      int maxWidthHeight = std::max(getConstDungeon().getWidth(), getConstDungeon().getHeight());
      return 3 * (maxWidthHeight < 16 ? 1.f : maxWidthHeight * 0.07f);
    };

  public:
    // this is called when we construct the class
    dungeon_app(int argc, char **argv) :
      app(argc, argv),
      _level(0),
      _dungeon(parse(argc, argv, _level)) {
        _inputManager.attach(this);
    };

    // this is called once OpenGL is initialized
    void app_init() {
      initialiseShaders();
      initialiseAudio();

      loadSprites();

      setState(&StartState::getInstance());
    };

    // this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      if (_state != NULL) {
        _state->operator()(this, x, y, w, h);
      }
    };
  };
}
